/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    container: {
      center: false,
      padding: {
        DEFAULT: "1.5rem",
        sm: "1.5rem",
        lg: "2rem",
        "2xl": "3rem",
      },
    },
    extend: {
      colors: {
        primaryColor: "#00b8ac",
        primarySoft: "#56d1c9",
        primaryDark: "#0eaca2",
        secondary: "#9E9E9E",
        error: "#ef4444",
        errorSoft: "#f76363",
        errorDark: "#c23838",
        warning: "#ffa500",
        warningSoft: "#f4cc83",
        warningDark: "#e39607",
        backgroundContent: "#fbfbfb",
        blackSoft: "rgba(0, 0, 0, 0.8)",
        subtitle: "#6C6C6C",
        yellow: {
          10: "#FFB200",
        },
        dark: {
          0: "#000000",
          1: "#202124",
          2: "#303134",
        },
        mint: {
          10: "#DCEFE6",
          500: "#387A59",
        },
        honey: {
          20: "#FFE7C8",
          500: "#D17500",
        },
        mono: {
          10: "#FFFFFF",
          30: "#EDEDED",
          40: "#E0E0E0",
          70: "#757575",
          80: "#616161",
          90: "#242424",
          100: "#141414",
          200: "#CBCBCB",
          500: "#828282",
          600: "#6C6C6C",
          900: "#2C2C2C",
        },
        light: {
          0: "#ffffff",
          1: "#e8eaed",
          2: "#bdc1c6",
          3: "#969ba1",
        },
      },
    },
  },
  extend: {},

  plugins: [],
};
