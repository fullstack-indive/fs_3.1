import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { LoginAction } from "@/store/reducers/auth/actions";
import { setError } from "@/store/reducers/auth";
import { Link } from "react-router-dom";
import { useEffect } from "react";

const Login = () => {
  const { errors, isLoading } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },

    onSubmit: (values) => {
      dispatch(LoginAction(values));
    },
  });

  useEffect(() => {
    dispatch(setError(null));
  }, []);

  return (
    <>
      <div className="flex justify-center items-center h-screen px-6">
        <div className="w-full sm:flex sm:justify-center">
          <div className="sm:border h-full sm:w-[480px] sm:p-12 rounded-2xl">
            <div className="text-center mb-4">
              <div className="font-semibold text-xl">Hello, Wellcome Back</div>
              <div className="text-sm text-gray-500">Login to your account</div>
            </div>
            <form onSubmit={formik.handleSubmit} className="mb-2">
              <div className="mb-3">
                <label htmlFor="email">Email</label>
                <input
                  id="email"
                  name="email"
                  type="email"
                  onChange={formik.handleChange}
                  value={formik.values.email}
                  placeholder="hdinjos@gmail.com"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.email?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.email?.map((p, i) => (
                    <span key={i}>{p}</span>
                  ))}
                </div>
              </div>
              <div className="mb-5">
                <label htmlFor="password">Password</label>
                <input
                  id="password"
                  name="password"
                  type="password"
                  onChange={formik.handleChange}
                  value={formik.values.password}
                  placeholder="j932jw32Dg"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.password?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.password?.map((p, i) => (
                    <span key={i}>{p}</span>
                  ))}
                  <div className="text-error text-sm">{errors?.message}</div>
                </div>
              </div>
              <div className="flex justify-center">
                <button
                  disabled={isLoading}
                  className={
                    "px-6 py-2 rounded text-light-0 w-full " +
                    (isLoading ? "bg-secondary" : "bg-primaryColor")
                  }
                  type="submit"
                >
                  {isLoading ? "Loading..." : "Login"}
                </button>
              </div>
            </form>

            <div className="text-center">
              Do not have an account?{" "}
              <Link
                to="/auth/register"
                className="text-secondary font-semibold"
              >
                Register
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
