// import { useEffect } from "react";
import { useEffect } from "react";
import { useSearchParams, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { VerifyAction } from "@/store/reducers/auth/actions";
import toast, { Toaster } from "react-hot-toast";

const EmailVerfication = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const token = searchParams.get("token");
  const email = searchParams.get("email");

  const handleVerify = async () => {
    try {
      await dispatch(VerifyAction({ email, token })).unwrap();
    } catch (err) {
      if (
        err.message === "Error token not found" ||
        err.message === "Your account is active"
      ) {
        toast.error(err?.message);
        setTimeout(() => {
          navigate("/auth/login", { replace: true });
        }, 1000);
      } else if (err.message === "Token has been expired") {
        navigate("/auth/resend-verification", {
          state: {
            email,
          },
        });
      } else {
        toast.error("An error ocured");
        setTimeout(() => {
          navigate("/auth/login", { replace: true });
        }, 1000);
      }
    }
  };
  useEffect(() => {
    handleVerify();
  }, []);

  return (
    <>
      <Toaster />
      <div className="flex justify-center items-center h-screen px-6 bg-mono-40">
        <div className="w-full sm:flex sm:justify-center">
          <div className="h-full sm:w-[480px] sm:p-12 rounded-2xl">
            <div className="text-center">Verify your account</div>
            <div className="text-center">Loading...</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EmailVerfication;
