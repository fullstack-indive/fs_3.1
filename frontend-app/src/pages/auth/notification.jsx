import { useState } from "react";
import { Link, useLocation } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import * as AuthServices from "@/services/AuthServices";

const Notification = () => {
  const { state } = useLocation();
  const [isLoading, setIsloading] = useState(false);
  const handleResend = async () => {
    try {
      setIsloading(true);
      await AuthServices.resendVerify({ email: state.email });
      toast.success("Success resend,check your email");
    } catch (err) {
      toast.error("Error resend");
    } finally {
      setIsloading(false);
    }
  };

  return (
    <>
      <Toaster />
      <div className="flex justify-center items-center h-screen px-6">
        <div className="w-full sm:flex sm:justify-center">
          <div className="sm:border h-full sm:w-[480px] sm:p-12 rounded-2xl">
            <div className="text-center mb-20">
              <div className="font-semibold text-2xl">Notification</div>
              <div className="text-xl text-primaryColor">
                Next, open mailbox in your email to verification
              </div>
            </div>
            <div className="text-center mb-2">
              You have not received a verification link in your email?
            </div>
            <button
              onClick={handleResend}
              disabled={isLoading}
              className={
                "px-6 py-2 rounded text-light-0 w-full mb-2 " +
                (isLoading ? "bg-secondary" : "bg-primaryColor")
              }
              type="submit"
            >
              {isLoading ? "Loading..." : "Resend to Email"}
            </button>
            <div className="text-center">
              Already have an account?{" "}
              <Link to="/auth/login" className="text-secondary font-semibold">
                Login
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Notification;
