import { useState } from "react";
import { useFormik } from "formik";
import { Link, useNavigate } from "react-router-dom";
import * as AuthServices from "@/services/AuthServices";

const Register = () => {
  const navigate = useNavigate();
  const [errors, setErrors] = useState(null);
  const [isLoading, setIsloading] = useState(false);

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
      password_confirmation: "",
    },

    onSubmit: async (values) => {
      try {
        setIsloading(true);
        await AuthServices.register(values);
        navigate("/auth/notification", { state: { email: values.email } });
      } catch (err) {
        setErrors(err?.response?.data);
      } finally {
        setIsloading(false);
      }
    },
  });

  return (
    <>
      <div className="flex justify-center items-center h-screen px-6">
        <div className="w-full sm:flex sm:justify-center">
          <div className="sm:border h-full sm:w-[480px] sm:p-12 rounded-2xl">
            <div className="text-center mb-4">
              <div className="font-semibold text-xl">Hello, Wellcome Back</div>
              <div className="text-sm text-gray-500">
                Register to use account
              </div>
            </div>
            <form onSubmit={formik.handleSubmit} className="mb-2">
              <div className="mb-3">
                <label htmlFor="name">Name</label>
                <input
                  id="name"
                  name="name"
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.name}
                  placeholder="Hendi Saputra"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.name?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.name?.map((p, i) => (
                    <div key={i}>{p}</div>
                  ))}
                </div>
              </div>
              <div className="mb-3">
                <label htmlFor="email">Email</label>
                <input
                  id="email"
                  name="email"
                  type="email"
                  onChange={formik.handleChange}
                  value={formik.values.email}
                  placeholder="hdinjos@gmail.com"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.email?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.email?.map((p, i) => (
                    <div key={i}>{p}</div>
                  ))}
                </div>
              </div>
              <div className="mb-3">
                <label htmlFor="password">Password</label>
                <input
                  id="password"
                  name="password"
                  type="password"
                  onChange={formik.handleChange}
                  value={formik.values.password}
                  placeholder="j932jw32Dg"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.password?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.password?.map((p, i) => (
                    <div key={i}>{p}</div>
                  ))}
                </div>
              </div>
              <div className="mb-5">
                <label htmlFor="password_confirmation">
                  Password Confirmation
                </label>
                <input
                  id="password_confirmation"
                  name="password_confirmation"
                  type="password"
                  onChange={formik.handleChange}
                  value={formik.values.password_confirmation}
                  placeholder="j932jw32Dg"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.password_confirmation?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.password_confirmation?.map((p, i) => (
                    <div key={i}>{p}</div>
                  ))}
                </div>
              </div>

              <div className="flex justify-center">
                <button
                  disabled={isLoading}
                  className={
                    "px-6 py-2 rounded text-light-0 w-full " +
                    (isLoading ? "bg-secondary" : "bg-primaryColor")
                  }
                  type="submit"
                >
                  {isLoading ? "Loading..." : "Register"}
                </button>
              </div>
            </form>

            <div className="text-center">
              Already have an account?{" "}
              <Link to="/auth/login" className="text-secondary font-semibold">
                Login
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Register;
