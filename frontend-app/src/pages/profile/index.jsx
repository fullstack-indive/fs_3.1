import { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import * as UserServices from "@/services/UserServices";
import { LogoutAction } from "@/store/reducers/auth/actions";
import { setUserName } from "@/store/reducers/auth";
import { useFormik } from "formik";
import toast, { Toaster } from "react-hot-toast";
import unfoto from "@/assets/images/un_profile.jpg";

const Profile = () => {
  const dispatch = useDispatch();

  const [errors, setErrors] = useState(null);
  const [isLoading, setIsloading] = useState(false);
  const [foto, setFoto] = useState(null);
  const [imgUrl, setImgUrl] = useState("");

  const handleLogout = () => {
    dispatch(LogoutAction());
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: "",
      foto_url: "",
      birth_date: "",
      phone: "",
      address: "",
      profession_a: false,
      profession_b: false,
      profession_c: false,
    },

    onSubmit: async (values) => {
      try {
        setIsloading(true);
        setErrors(null);
        const profession = [
          values.profession_a ? "a" : "",
          values.profession_b ? "b" : "",
          values.profession_c ? "c" : "",
        ]
          .filter((item) => item !== "")
          .toString();

        const request = {
          name: values.name,
          foto,
          birth_date: values.birth_date,
          phone: values.phone.toString(),
          address: values.address,
          profession,
        };

        if (!foto) {
          delete request.foto;
        }

        let bodyFormData = new FormData();
        for (let r in request) {
          bodyFormData.append(r, request[r]);
        }

        await UserServices.update(bodyFormData);
        await fetchUser();
        dispatch(setUserName(values.name));
        toast.success("Update profile success");
      } catch (err) {
        setErrors(err?.response.data);
      } finally {
        setIsloading(false);
      }
    },
  });

  const fetchUser = async () => {
    const response = await UserServices.get();
    const { data } = response.data;
    setImgUrl(data?.foto_url);
    formik.setValues({
      name: data?.name,
      birth_date: data?.birth_date.split("-").reverse().join("/"),
      phone: data?.phone,
      address: data?.address,
      profession_a: data?.profession_a,
      profession_b: data?.profession_b,
      profession_c: data?.profession_c,
    });
  };

  useEffect(() => {
    fetchUser();
  }, []);

  const inputRef = useRef();
  const handleClickImage = () => {
    inputRef.current.click();
  };

  const uploadImage = async (event) => {
    const file = event.target.files?.length > 0 ? event.target.files[0] : null;
    const reader = new FileReader();
    reader.addEventListener("load", function () {
      setImgUrl(reader.result);
      setFoto(file);
    });
    reader.readAsDataURL(file);
  };
  return (
    <>
      <Toaster />
      <div className="flex justify-center items-center h-screen px-6">
        <div className="w-full sm:flex sm:justify-center">
          <div className="sm:border h-full sm:w-[480px] sm:p-12 rounded-2xl">
            <div className="text-center mb-4">
              <div className="font-semibold text-xl">Profile</div>
              <div
                onClick={handleLogout}
                className="text-sm text-gray-500 cursor-pointer"
              >
                Logout
              </div>
            </div>
            <input
              ref={inputRef}
              type="file"
              hidden
              onChange={uploadImage}
              accept=".jpg,.png,.jpeg"
            />
            <form onSubmit={formik.handleSubmit} className="mb-2">
              <div className="mb-3">
                <div>Foto</div>
                {!imgUrl ? (
                  <img
                    src={unfoto}
                    alt="profile_img"
                    className="h-32 w-32 rounded-lg cursor-pointer"
                    onClick={handleClickImage}
                  />
                ) : (
                  <img
                    src={imgUrl}
                    className="h-32 w-32 rounded-lg"
                    alt="profile_img"
                    onClick={handleClickImage}
                  />
                )}
                <div className="text-error text-sm">
                  {errors?.data?.foto?.map((p, i) => (
                    <div key={i}>{p}</div>
                  ))}
                </div>
              </div>
              <div className="mb-3">
                <label htmlFor="name">Name</label>
                <input
                  id="name"
                  name="name"
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.name}
                  placeholder="Hendi Saputra"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.name?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.name?.map((p, i) => (
                    <div key={i}>{p}</div>
                  ))}
                </div>
              </div>
              <div className="mb-3">
                <label htmlFor="name">Phone</label>
                <input
                  id="phone"
                  name="phone"
                  type="number"
                  onChange={formik.handleChange}
                  value={formik.values.phone}
                  placeholder="082XXXXXXXX"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.phone?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.phone?.map((p, i) => (
                    <div key={i}>{p}</div>
                  ))}
                </div>
              </div>

              <div className="mb-3">
                <label htmlFor="birth_date">Birth Date</label>
                <input
                  id="birth_date"
                  name="birth_date"
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.birth_date}
                  placeholder="05/11/1996"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.birth_date?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.birth_date?.map((p, i) => (
                    <div key={i}>{p}</div>
                  ))}
                </div>
              </div>

              <div className="mb-3">
                <label htmlFor="address">Address</label>
                <textarea
                  id="address"
                  name="address"
                  onChange={formik.handleChange}
                  value={formik.values.address}
                  placeholder="Jl. Kaliurang Km.5, Yogyakarta"
                  className={
                    "border border-mono-70 rounded px-3 py-2 w-full focus:outline-none focus:ring-0 " +
                    (errors?.data?.address?.length > 0
                      ? "border-error"
                      : "focus:border-primaryColor")
                  }
                />
                <div className="text-error text-sm">
                  {errors?.data?.address?.map((p, i) => (
                    <div key={i}>{p}</div>
                  ))}
                </div>
              </div>

              <div className="mb-5">
                <div className="mb-1">Profession</div>
                <div className="mb-1">
                  <input
                    id="profession_a"
                    name="profession_a"
                    type="checkbox"
                    onChange={formik.handleChange}
                    checked={formik.values.profession_a}
                    placeholder="Hendi Saputra"
                    className={"mr-2"}
                  />
                  <label htmlFor="profession_a">Fullstack Developer</label>
                </div>
                <div className="mb-1">
                  <input
                    id="profession_b"
                    name="profession_b"
                    type="checkbox"
                    onChange={formik.handleChange}
                    checked={formik.values.profession_b}
                    placeholder="Hendi Saputra"
                    className={"mr-2"}
                  />
                  <label htmlFor="profession_b">Frontend Developer</label>
                </div>
                <div className="mb-1">
                  <input
                    id="profession_c"
                    name="profession_c"
                    type="checkbox"
                    onChange={formik.handleChange}
                    checked={formik.values.profession_c}
                    placeholder="Hendi Saputra"
                    className={"mr-2"}
                  />
                  <label htmlFor="profession_c">Backend Developer</label>
                  <div className="text-error text-sm">
                    {errors?.data?.profession?.map((p, i) => (
                      <div key={i}>{p}</div>
                    ))}
                  </div>
                </div>
              </div>

              <div className="flex justify-center">
                <button
                  disabled={isLoading}
                  className={
                    "px-6 py-2 rounded text-light-0 w-full " +
                    (isLoading ? "bg-secondary" : "bg-primaryColor")
                  }
                  type="submit"
                >
                  {isLoading ? "Loading..." : "Update"}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Profile;
