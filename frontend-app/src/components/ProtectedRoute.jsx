import { Navigate, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
const ProtectedRoute = ({ children }) => {
  const token = useSelector((state) => state.auth.token);
  const { pathname } = useLocation();
  const loginUrl = "/auth/login";

  if (pathname === loginUrl) {
    if (token) {
      return <Navigate to="/" />;
    }
    return <>{children}</>;
  }
  if (token) {
    return <>{children}</>;
  } else {
    return <Navigate to={loginUrl} />;
  }
};
export default ProtectedRoute;
