import { useEffect } from "react";
import { useDispatch } from "react-redux";
import toast, { Toaster } from "react-hot-toast";
import { resetLogin } from "@/store/reducers/auth";
import { router } from "@/routers";

const ErrorBoundary = ({ children }) => {
  const dispatch = useDispatch();
  const pathname = window.location.pathname;

  const getErr = async (event) => {
    console.log(event?.reason);
    if (event?.reason?.response?.status === 401) {
      toast.success("Your Session is expired, please login again");
      dispatch(resetLogin());
      router.navigate("/auth/login", { replace: true });
    } else {
      toast.error("An Error Ocured");
    }
  };

  useEffect(() => {
    if (pathname !== "/login") {
      window.addEventListener("unhandledrejection", getErr);
    } else {
      return () => {
        window.removeEventListener("unhandledrejection", getErr);
      };
    }
    return () => {
      window.removeEventListener("unhandledrejection", getErr);
    };
  }, [pathname]);

  return (
    <>
      <Toaster />
      {children}
    </>
  );
};

export default ErrorBoundary;
