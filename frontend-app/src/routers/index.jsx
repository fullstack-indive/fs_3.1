import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Login from "../pages/auth/login";
import Register from "../pages/auth/register";
import Profile from "@/pages/profile";
import EmailVerification from "../pages/auth/email-verification";
import Notification from "../pages/auth/notification";
import ResendVerification from "@/pages/auth/resend-verification";
import ErrorPage from "../components/ErrorPage";
import ProtectedRoute from "@/components/ProtectedRoute";
import ErrorBoundary from "../components/ErrorBoundary";

export const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <ProtectedRoute>
        <Profile />
      </ProtectedRoute>
    ),
    errorElement: <ErrorPage />,
  },
  {
    path: "/auth/login",
    element: (
      <ProtectedRoute>
        <Login />
      </ProtectedRoute>
    ),
  },
  {
    path: "/auth/register",
    element: <Register />,
  },
  {
    path: "/auth/email-verification",
    element: <EmailVerification />,
  },
  {
    path: "/auth/notification",
    element: <Notification />,
  },

  {
    path: "/auth/resend-verification",
    element: <ResendVerification />,
  },
]);

const Router = () => {
  return (
    <ErrorBoundary>
      <RouterProvider router={router} />;
    </ErrorBoundary>
  );
};

export default Router;
