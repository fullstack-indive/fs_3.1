import { createAsyncThunk } from "@reduxjs/toolkit";
import * as AuthService from "@/services/AuthServices";

export const LoginAction = createAsyncThunk(
  "auth/login",
  async (params, { rejectWithValue }) => {
    try {
      const { data } = await AuthService.login(params);
      return data;
    } catch (err) {
      if (err?.response?.status === 400 || err?.response?.status === 404) {
        return rejectWithValue(err.response?.data);
      } else {
        throw new Error("An error occurred");
      }
    }
  }
);

export const LogoutAction = createAsyncThunk("auth/logout", async () => {
  try {
    await AuthService.logout();
  } catch (err) {
    if (err?.response?.status === 400 || err?.response?.status === 404) {
      throw err.response.data.message;
    } else {
      throw new Error("An error occurred");
    }
  }
});

export const VerifyAction = createAsyncThunk("auth/verify", async (params) => {
  try {
    const { data } = await AuthService.verifyAccount(params);
    return data;
  } catch (err) {
    if (err?.response?.status === 400 || err?.response?.status === 404) {
      throw err.response.data.message;
    } else {
      throw new Error("An error occurred");
    }
  }
});
