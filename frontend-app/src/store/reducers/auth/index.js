import { createSlice } from "@reduxjs/toolkit";
import {
  LoginAction,
  LogoutAction,
  VerifyAction,
} from "@/store/reducers/auth/actions.js";
import { router } from "@/routers";

const initialState = {
  token: "",
  user: {},
  isLoading: false,
  errors: null,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    resetLogin: () => initialState,
    setToken: (state, action) => {
      state.token = action.payload;
    },
    setUser: (state, action) => {
      state.user = action.payload;
    },
    setUserName: (state, action) => {
      state.user.name = action.payload;
    },
    setError: (state, action) => {
      state.errors = action.payload;
    },
    setIsLoading: (state, action) => {
      state.isLoading = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(LoginAction.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(LoginAction.rejected, (state, action) => {
        state.isLoading = false;
        state.errors = action.payload;
        // message.error("Login Gagal! "  + action?.error?.message);
      })
      .addCase(LoginAction.fulfilled, (state, action) => {
        state.isLoading = false;
        state.token = action.payload.access_token;
        state.user = action.payload.user;
        router.navigate("/");
      });

    builder
      .addCase(LogoutAction.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(LogoutAction.rejected, (state) => {
        state.isLoading = false;
        // message.error("Login Gagal! "  + action?.error?.message);
      })
      .addCase(LogoutAction.fulfilled, (state) => {
        state.isLoading = false;
        state.token = "";
        state.user = {};
      });

    builder
      .addCase(VerifyAction.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(VerifyAction.rejected, (state) => {
        state.isLoading = false;
      })
      .addCase(VerifyAction.fulfilled, (state, action) => {
        state.isLoading = false;
        state.token = action.payload.access_token;
        state.user = action.payload.user;
        router.navigate("/");
      });
  },
});

// Action creators are generated for each case reducer function
export const {
  resetLogin,
  setToken,
  setUser,
  setUserName,
  setError,
  setIsLoading,
} = authSlice.actions;

export default authSlice.reducer;
