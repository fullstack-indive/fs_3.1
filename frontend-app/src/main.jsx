// import React from "react";
import ReactDOM from "react-dom/client";
// import App from './App.jsx'
import Router from "./routers";
import { store, persistor } from "./store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { injectStore } from "./services/config";
import "./assets/css/tailwind.css";

injectStore(store);

ReactDOM.createRoot(document.getElementById("root")).render(
  // <React.StrictMode>
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router />
    </PersistGate>
    {/* <App /> */}
  </Provider>
  // </React.StrictMode>
);
