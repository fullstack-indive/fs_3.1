import xhr from "@/services/config";

const get = () => xhr.get("/user-info");
const update = (params) =>
  xhr.post("/user", params, {
    headers: { "Content-Type": "multipart/form-data" },
  });

export { get, update };
