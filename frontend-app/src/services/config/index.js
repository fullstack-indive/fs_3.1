import axios from "axios";
import { resetLogin } from "@/store/reducers/auth";

// to inject store redux
let store;

export const injectStore = (_store) => {
  store = _store;
};

const instance = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_URL_API,
  timeout: 60000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

// Add a request interceptor
instance.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    const { auth } = store.getState();

    if (auth ? Object.keys(auth?.user).length > 0 : false) {
      // const lastLoginParse = JSON.parse(lastLogin);
      config.headers.Authorization = "Bearer " + auth?.token;
    } else {
      delete config.headers.Authorization;
    }
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
instance.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    if (
      error.response.status === 401 &&
      error.response.statusText === "Unauthorized"
    ) {
      store.dispatch(resetLogin());
    }

    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);

export default instance;
