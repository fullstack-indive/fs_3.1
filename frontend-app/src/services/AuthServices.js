import xhr from "@/services/config";

const login = (params) => xhr.post("/auth/login", params);
const register = (params) => xhr.post("/auth/register", params);
const logout = () => xhr.get("/auth/logout");
const resendVerify = (params) => xhr.post("/auth/resend-verification", params);
const verifyAccount = (params) => xhr.post("/auth/verify-account", params);

export { login, register, logout, resendVerify, verifyAccount };
