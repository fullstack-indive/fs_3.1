## Running Project

## backend -app

1. composer update
2. copas .env.example ke .env yang ada di root project folder backend-app

3. buat database baru di postgresql (namanya profile atau sesuai settingan kamu)
4. setting .env

```sh
//sesuaikan dbmsnya
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=profile
DB_USERNAME=hdinjos
DB_PASSWORD=123

//smtp emailnya, ini pakai akun saya, sesuaikan dengan punyamu (buat tes verifikasi email)
MAIL_MAILER=smtp
MAIL_HOST=sandbox.smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=fe2cfba9529f06
MAIL_PASSWORD=1956c355dbb814
```

5. generate key laravel (php artisan key:generate)
6. generate key jwt (php artisan jwt:secret)
7. link storage ke public (php artisan storage:link)
8. migrate laravel (php artisan migrate)
9. jalankan laravel (php artisan serve)

## frontend -app

1. node versi 20 (lts)
2. copas .env.example ke .env yang ada di root project folder frontend-app
3. setting .env

```sh
   VITE_APP_BASE_URL_API=http://localhost:8000/api/v1
```

5. install dependencies (npm install) di root project frontend-app
6. jalankan project (npm run dev)

## referensi tools yang saya pakai

```
❯ php --version
PHP 8.1.27 (cli) (built: Dec 21 2023 20:19:54) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.1.27, Copyright (c) Zend Technologies
    with Zend OPcache v8.1.27, Copyright (c), by Zend Technologies
❯ node --version
v20.11.0
❯ npm --version
10.2.4
❯ composer --version
Composer version 2.7.1 2024-02-09 15:26:28

```

## Dokumentasi API dengan postman

-Dokumentasinya ada di folder dokumentasi-postman
-filenya profile.postman_collection.json
-gunakan collection versi 2.1
