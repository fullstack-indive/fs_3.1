<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Authentication;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
// 	return $request->user();
// });


Route::group([
	'prefix' => '/v1/auth'
], function ($router) {
	Route::post("/register", [Authentication::class, "register"]);
	Route::post("/login", [Authentication::class, "login"]);
	Route::post("/verify-account", [Authentication::class, "verifyAccount"]);
	Route::post("/resend-verification", [Authentication::class, "resendVerificationLink"]);
	Route::get("/logout", [Authentication::class, "logout"])->middleware("jwt.verify");
});

Route::group([
	'middleware' => ['jwt.verify'],
	'prefix' => '/v1'
], function ($router) {
	Route::get("/user-info", [UserController::class, "index"]);
	Route::post("/user", [UserController::class, "update"]);
});
