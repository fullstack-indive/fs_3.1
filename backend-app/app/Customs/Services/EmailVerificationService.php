<?php

namespace App\Customs\Services;

use App\Models\EmailVerificationToken;
use App\Notifications\EmailVerificationNotification;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;

class EmailVerificationService
{
	/*
	 * Send verification link to user
	 *
	 *
	 */
	public function sendVerificationLink($user)
	{
		Notification::send($user, new EmailVerificationNotification($this->generateVerificationLink($user->email)));
	}

	/*
	 *Generate verification link
	 */
	public function generateVerificationLink($email)
	{
		$checkTokenIfExists = EmailVerificationToken::where("email", "=", $email)->first();
		if ($checkTokenIfExists) {
			$checkTokenIfExists->delete();
		}

		$newToken = Str::uuid();
		$saveToken =  EmailVerificationToken::create([
			"email" => $email,
			"token" => $newToken,
			"expired_at" => Carbon::now()->addMinute(10),
		]);

		$url = "http://localhost:5173/auth/email-verification?token=" . $newToken . "&email=" . $email;

		if ($saveToken) {
			return $url;
		}
	}
}
