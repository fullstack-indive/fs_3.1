<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\User;

class UserController extends Controller
{
	function index()
	{
		$userId = auth()->user()->id;
		$user = User::find($userId);
		return response()->json([
			"success" => true,
			"data" => $user
		]);
	}

	function update(Request $request)

	{
		$validator = Validator::make($request->all(), [
			'name' => 'required|string|between:2,100',
			'foto' => 'image|mimes:png,jpg,jpeg,svg|max:2048',
			"birth_date" => "required|date_format:d/m/Y",
			"phone" => "required|string",
			"address" => "required|string",
			"profession" => "required|string",
		], ["birth_date" => "format date is dd/mm/yyyy"]);


		if ($validator->fails()) {
			return response()->json(
				[
					"success" => false,
					"data" => $validator->errors()
				],
				422
			);
		}

		$professionA = str_contains($request->profession, "a") ? true : false;
		$professionB = str_contains($request->profession, "b") ? true : false;
		$professionC = str_contains($request->profession, "c") ? true : false;
		$professions = [
			"profession_a" => $professionA,
			"profession_b" => $professionB,
			"profession_c" => $professionC,
		];
		$userId = auth()->user()->id;
		$findUser = User::find($userId);

		if (!$findUser) {
			return response()->json(
				[
					"success" => false,
					"message" => "user not found"
				],
				404
			);
		}

		$dateParse = explode("/", $request->birth_date);
		$newRequest = array(
			'name' => $request->name,
			"birth_date" => implode("-", array_reverse($dateParse)),
			"phone" => $request->phone,
			"address" => $request->address,
		);


		if ($request->hasFile("foto")) {
			var_dump($request->foto);
			$foto = $request->file("foto");
			$fotoName = $foto->hashName();

			if (Storage::exists($findUser->foto_location)) {
				Storage::delete($findUser->foto_location);
				$foto->storeAs("public/profile", $fotoName);
			} else {
				$foto->storeAs("public/profile", $fotoName);
			}
			$reqUpdate = array_merge(array_diff($newRequest, ["profession" => $request->profession]), array_merge($professions, ["foto" => $fotoName]));
			$findUser->update($reqUpdate);
			return response()->json([
				"success" => true,
				"message" => "update user success"
			]);
		} else {
			$reqUpdate = array_merge(array_diff($newRequest, ["profession" => $request->profession]), $professions);
			$findUser->update($reqUpdate);
			return response()->json([
				"success" => true,
				"message" => "update user success"
			]);
		}
	}
}
