<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Models\EmailVerificationToken;
use App\Customs\Services\EmailVerificationService;

class Authentication extends Controller
{
	public 	function __construct(private EmailVerificationService $service)
	{
	}

	function register(Request $request)
	{
		$validation = Validator::make($request->all(), [
			"name" => "required|string",
			"email" => "required|email|unique:users,email",
			"password" => "required|confirmed|min:6",
			"password_confirmation" => "required|min:6",
		]);

		if ($validation->fails()) {
			return response()->json(
				[
					"sucess" => false,
					"data" => $validation->errors()
				],
				400
			);
		}
		$user = User::create([
			"name" => $request->name,
			"email" => $request->email,
			"password" => Hash::make($request->password),
		]);

		if ($user) {
			$this->service->sendVerificationLink($user);
			return response()->json([
				"sucess" => true,
				"message" => "register success, please see your email to verification account",
			]);
		} else {
			return response()->json(
				[
					"sucess" => false,
					"message" => "Error register",
				],
				500
			);
		}
	}

	function login(Request $request)
	{
		$validation = Validator::make($request->all(), [
			"email" => "required",
			"password" => "required"
		]);

		if ($validation->fails()) {
			return response()->json(
				[
					"success" => false,
					"data" => $validation->errors()
				],
				400
			);
		}

		if ($validation->validate()) {
			$pass = auth()->attempt($validation->validate());
			if (!$pass) {
				return response()->json(
					[
						"success" => false,
						"message" => "invalid credentials",
					],
					400
				);
			}

			if (!auth()->user()->email_verified_at) {
				auth()->logout();
				return response()->json(
					[
						"success" => false,
						"message" => "this account is not verified",
					],
					400
				);
			}

			$token = auth()->login(auth()->user());
			return $this->resWithToken($token, auth()->user(), "Login success");
		}
	}

	function verifyAccount(Request $request)
	{

		$user = User::where("email", "=", $request->email)->where("email_verified_at", "!=", NULL)->first();
		if ($user) {
			return response()->json([
				"success" => false,
				"message" => "Your account is active",
			], 400);
		}


		$checkTokenExist = EmailVerificationToken::where("email", "=", $request->email)
			->where("token", "=", $request->token)
			->first();

		if (!$checkTokenExist) {
			return response()->json([
				"success" => false,
				"message" => "Error token not found",
			], 404);
		}

		$compareExp = Carbon::now()->greaterThan(Carbon::parse($checkTokenExist->expired_at));

		if ($compareExp) {
			return response()->json([
				"success" => false,
				"message" => "Token has been expired",
			], 400);
		}
		$checkTokenExist->delete();

		$user = User::where("email", "=", $request->email)->first();
		$user->email_verified_at = Carbon::now();
		$user->save();

		$token = auth()->login($user);

		return $this->resWithToken($token, $user, "Verification account success");
	}

	function resendVerificationLink(Request $request)
	{
		$validation = Validator::make($request->all(), [
			"email" => "required",
		]);


		if ($validation->fails()) {
			return response()->json([
				"success" => false,
				"message" => "Error data input",
				"data" => $validation->errors()
			], 400);
		}

		$user = User::where("email", "=", $request->email)->first();
		if ($user) {
			$this->service->sendVerificationLink($user);
			return response()->json([
				"success" => true,
				"message" => "resend verfication link success. please, check your email",
			], 201);
		} else {
			return response()->json([
				"success" => false,
				"message" => "User not found",
			], 404);
		}
	}


	function logout()
	{
		if (auth()->user()) {
			auth()->logout();
			return response()->json([
				"success" => true,
				"message" => "success logout"
			]);
		} else {
			return response()->json([
				"success" => false,
				"message" => "no session"
			]);
		}
	}

	function resWithToken($token, $user, $message)
	{
		return response()->json([
			"success" => true,
			"message" => $message,
			"access_token" => $token,
			"type" => "bearer",
			"user" => [
				"id" => $user->id,
				"name" => $user->name,
				"email" => $user->email,
			]
		]);
	}
}
